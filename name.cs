﻿using System;

namespace ex_13
{
    class Program
    {
        static void Main(string[] args)
        {
           var myName = "Nick";
           Console.WriteLine(myName);

           //End the program with blank line and instructions
           Console.ResetColor();
           Console.WriteLine();
           Console.WriteLine("Press <Enter> to quit the program");
           Console.ReadKey();

        }
    }
}
